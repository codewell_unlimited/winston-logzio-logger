const logger = require('./config');
const utils = require('./utils');

const log = caller => {
	return {
		info: message => logger.info(`[${utils.getFilePath(caller)}] | ${message}`),
		debug: message => logger.debug(`[${utils.getFilePath(caller)}] | ${message}`),
		error: message => logger.error(`[${utils.getFilePath(caller)}] | ${message}`)
	}
};

module.exports = {
	log
};