const getFilePath = caller => {
	if (!caller || caller.length === 0)
		return 'unknown file';

	var paths;
	var length;

	if (caller.includes('/')) {
		paths = caller.split('/');
		length = paths.length - 1;
	} else if (caller.includes('\\')) {
		paths = caller.split('\\');
		length = paths.length - 1;
	}

	return paths[length - 1] + '/' + paths[length];
};


module.exports = {
	getFilePath
};