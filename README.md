# Winston Logzio Logger

A custom logger for CodeWell's NodeJS Projects.

## Getting Started

### What is this repository for?

This is a custom configured logger for our NodeJS projects.

### Built With
* Winston logger (https://github.com/winstonjs/winston)
* Winston-logzio transport

### How do I get set up?

Go to your project and run this command from your command line:
```
npm install https://bitbucket.org/codewell_unlimited/winston-logzio-logger
```

### Prerequisites

* Create an account on Logzio (https://logz.io)
* Create an .env file (environment) and set these two variables:
	* LOGZIO_TOKEN="YOUR_TOKEN"
	* LOGZIO_HOST="listener.logz.io"

### How do I use it?
* Load the module

```
const log = require('winston-logzio-logger').log(__filename);
```

* Log info message

```
log.info('This is an info log');
```

* Log debug message

```
log.debug('This is a debug log');
```

* Log error message

```
log.error('This is an error log');
```

* Log using variables

```
var info_message = 'This is a great info message!';
```

```
var error_message = 'Offfffffff';
```



```
log.info(`Let's log this info message: ${info_message}`);
```

```
log.error(`There's a bug in your code... ${error_message}`);
```

### Who do I talk to?

* Simona Andrieska: simona.andrieska@codewell.mk
* Martin Ancevski: martin@codewell.is