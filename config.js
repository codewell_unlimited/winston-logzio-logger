const fs = require('fs');
const winston = require('winston');

const {
	format,
	createLogger
} = winston;

const {
	combine,
	timestamp,
	printf
} = format;

const env = process.env.NODE_ENV || 'development';

const directory = 'log';

if (!fs.existsSync(directory))
	fs.mkdirSync(directory);

var transports = {
	Console: new winston.transports.Console(),
	File: new(require('winston-daily-rotate-file'))({
		timestamp,
		filename: `${directory}/-results.log`,
		datePattern: 'YYYY-MM-DD',
		prepend: true
	}),
	Logzio: new(require('winston-logzio'))({
		token: process.env.LOGZIO_TOKEN,
		host: process.env.LOGZIO_HOST,
	})
};

const logger = createLogger({
	levels: {
		'error': 0,
		'debug': 1,
		'info': 2
	}
});

if (env === 'production')
	logger.add(transports.Logzio);
else {
	logger.add(transports.Console);
	logger.add(transports.File);
}

logger.format = combine(
	format.splat(),
	format.colorize(),
	timestamp(),
	printf(info =>
		`${(new Date()).toLocaleString()} | ${info.level} | ${info.message}`)
);

process.on('uncaughtException', err =>
	logger.error(`UncaughtException ${err}`));

module.exports = logger;